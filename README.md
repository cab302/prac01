CAB302 Software Development
===========================

# Practical 1: Introducing Java development in an IDE

This week's practical exercises aim to get you familiar with writing and debugging simple Java programs using an integrated development environment (IDE).

* * *

## Exercise 0: Getting Started
To begin this exercise you need a Java IDE. Popular free IDEs are:

* [IntelliJ IDEA](https://www.jetbrains.com/idea/) (get the Community version)
* [Eclipse](https://www.eclipse.org/)
* [NetBeans](https://netbeans.org/)

Note that many of the examples and instructions in this unit use IntelliJ IDEA, and assignments will be loaded in and marked using IDEA, so this should be your primary choice.

Once you have installed and set up your IDE, import the Practical 1 project into your IDE. There are a couple of ways you can go about doing this, but they all require [Git](https://git-scm.com/) so make sure you have that installed before beginning.

- Open up your Git shell (or just a regular shell if on Linux or Mac), navigate to the directory where you want to keep your practical work, then type `git clone https://bitbucket.org/cab302/prac01.git`. You can get this command by clicking the Clone button at the top of the Bitbucket page, as it will be different for each practical.
- In IDEA, go to File -> New -> Project from Version Control -> Git, then paste in just the URL from above (`https://bitbucket.org/cab302/prac01.git`). If IDEA is unable to locate Git automatically, you will need to tell it where your Git binary is (File -> Settings -> Version Control -> Git -> Path to Git executable).

* * *

## Exercise 1: A message decoder

As a first exercise involving iteration and console output in Java you will develop a small program to decode a secret message. In the `prac01` project, create a new _package_ (e.g., '`answers`') (right-click on `src` and New -> Package). Then create a new _class_ called '`Decoder`' (right-click on the new package and New -> Class).

You should now see an editing window with an empty program template something like this:

![Empty template program](imgs/decoder.png "Empty template program")

The first thing you will need to do is create a `main` method that will be the entry point to your application. That will normally look like this:

    public static void main(String[] args) {
    
    }

You can then begin writing code between the two braces.

The challenge from this point is to create a program which will decode a "secret message". The message is encoded as an array of numbers, declared as the following constant:

    /* The secret message */
    final int[] message = { 82, 96, 103, 103, 27, 95, 106, 105, 96, 28 };

It can be unlocked with a particular key, declared as follows:

    /* The key for unlocking the message */
    final int key = 5;

Copy these two declarations into your main program. Then, to unlock the message, you need to write Java code that will:

1. add the key to each number in the message;
2. convert each number in the message to its equivalent character representation (most easily done by type-casting the numbers to __`char`__ type); and
3. print each of the resulting characters (which is most easily done using method `System.out.print()`, [other methods of `System.out` are listed here](https://docs.oracle.com/en/java/javase/11/docs/api/java.base/java/io/PrintStream.html#method.summary)).

Ask your tutor if you're not sure how to do any of these things.

As you edit your program, take note of how your IDE generates error and warning messages for incomplete or incorrect code segments. In particular, try out the 'intention action' feature by clicking on an error or warning icon or pressing alt+enter, and selecting a suitable solution for your problem, if one is available.

**Hint:** As usual when programming, _think_ before you type. This exercise can be solved very concisely.

**Hint:** This is a good opportunity to use a 'for each' loop.

**Extra exercise:** If time permits, develop a corresponding 'encoder' program that prints a string as a sequence of numbers. This time the key should be _subtracted_ from the number corresponding to each character. You may find a helpful method or two in Java's `String` class -- look it up in the [Java API Specification](https://docs.oracle.com/en/java/javase/11/docs/api/index.html). (You'll be spending a lot of time browsing this document in coming weeks!)

* * *

## Exercise 2: Debugging a 'summation' program

This exercise will give you practice in using IDEA's debugger. Get the Java program `Summation.java` (it is part of the Git repository that contained this file). This program is meant to calculate the sum of the natural numbers in a given range. The range is expressed by constants for the starting and finishing numbers, inclusive. Recall, for instance that the sum of `i` over 1<=`i`<=5 is 15.

Load the program into your project, either: by copying it directly into your project using your operating system's file browser; by creating a new class and copying the program text into IDEA; or by dragging it from the file browser into the package in IDEA. (No matter how you do it, make sure that the __`package`__ declaration at the beginning of the program matches the name of your package, otherwise you will get a compilation error.)

**(a)** Run the program with a `START` value of 1 and an `END` value of 5. Is the answer what you expected? In the `add()` method click in the grey bar just to the left of the __`for`__ loop statement (in the space between the line numbers and the code). This adds a _breakpoint_ to the application. Now 'debug' rather than 'run' the program. You will be able to control the program's execution using the various debugging buttons ![various debugging icons](imgs/debugbtns.png "various debugging icons"). Step through the program and watch how variables `index` and `total` change. By doing this you should be able to spot and fix the coding error.

**Note:** If you hover the cursor over a variable when the program is stopped the variable's value is displayed.

**(b)** Having corrected the error in the program, now change the value of constant `END` to 1000. Run the program. Does the answer look correct? Debug the program again and see if you can identify and fix this new problem. (This may require some patience!)

* * *

## Exercise 3: A 'decision making' method

This exercise will give you practice at writing a subroutine (method) in Java involving a conditional (__`if`__ or __`switch`__) statement. Quadcharts are a commonly-used technique for making decisions involving two independent variables. For instance, the following quadchart suggests the response you should make when asked by your boss to complete a job, given its level of difficulty and expected duration.

.  | **Easy Job** | **Hard Job**
--:|:----------:|:---------:
**Short Deadline** | Discuss | Decline
**Long Deadline** | Accept | Discuss

If the job is easy and there is plenty of time to complete it you should accept. If the job is hard and there is very little time you should decline. If the job is easy but the deadline is short you should negotiate for an extension of time. If there is plenty of time to complete the work but the job is hard you should negotiate for more staff.

As a management aid, your task is to define a function that accepts estimates of a job's duration (in months) and difficulty (on a scale of 1 to 10) and returns a string indicating which of the three possible responses you should make, "`accept`", "`decline`" or "`discuss`". ___Jobs with a duration of three months or less are considered 'short' and jobs with a difficulty level of seven or more are considered
'hard'___. (To simplify the solution you can hardwire these values as constants into your
program, although this is not normally good programming practice.)

To start, get the Java file `DecisionMaker.java` from the repository. This program template contains a '`main`' method which serves to test the desired `decision` method. Your task is to complete the program by writing the missing `decision` method so that the program produces the results 'accept', 'decline', 'discuss' and 'discuss' for the four proposed projects, respectively. (Later in this unit we will introduce a far more systematic way of testing programs.)

**Observation:** This exercise is good practice at writing conditional statements. Making a three-way decision based on two variables is awkward, no matter how it's coded.